import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

import * as WorldWind from '@nasaworldwind/worldwind';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.sass']
})
export class MapComponent implements AfterViewInit  {
  @ViewChild('scene') scene: ElementRef;

  wwd: any;

  ngAfterViewInit() {
    // const wwd = WorldWind.WorldWindow(this.scene.nativeElement);
    // This will work with the next release of WebWorldWind, which supports an
    // actual element instead of the ID as a string.

    // In the meantime, the ID must be used and makes the component not easily
    // reusable.
    
    this.wwd = new WorldWind.WorldWindow('scene');

    this.wwd.addLayer(new WorldWind.BMNGOneImageLayer());
    this.wwd.addLayer(new WorldWind.CoordinatesDisplayLayer(this.wwd));

  }

}
